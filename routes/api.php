<?php

Route::get('wedstrijden', 'WedstrijdController@index');
Route::prefix('wedstrijden/{id}/')->group(function () {
    Route::get('', 'WedstrijdController@get');
    Route::get('categorieen', 'WedstrijdController@categorieen');
    Route::get('uitslagen/{categorie_id}', 'WedstrijdController@uitslagen');
    Route::get('startnummer/{startnummer}', 'WedstrijdController@deelnemer');
});

