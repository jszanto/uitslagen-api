<?php

namespace App\Http\Controllers;


use App\Models\Categorie;
use App\Models\UitslagDeelnemer;
use App\Models\UitslagDeelnemerRealTime;
use App\Models\Wedstrijd;
use Illuminate\Http\Request;

class WedstrijdController extends Controller
{
    public function index(Request $request) {
        $this->validate($request, ['seizoen' => 'integer']);
        if ($request->has('seizoen')) {
            $jaar = $request->get('seizoen');
        } else {
            // The season starts in September, so to get the current season we check which month it is
            $jaar = date('n') < 9 ? date('Y') - 1 : date('Y');
        }
        return Wedstrijd::where('jaar', $jaar)->get();
    }

    public function get($id) {
        return Wedstrijd::findOrFail($id);
    }

    public function categorieen($id) {
        return Categorie::where('run_id', $id)->get();
    }

    public function uitslagen($id, $categorie_id)
    {
        // TODO: do something with categories and filtering
        // Verify wedstrijd exists
        $wedstrijd = $this->get($id);
        $deelnemers = UitslagDeelnemer::where('wedstrijd_id', $id);
        $status = 'definitief';
        if ($deelnemers->count() == 0) {
            $status = 'voorlopig';
            $deelnemers = UitslagDeelnemerRealTime::where('wedstrijd_id', $id);
        }

        return [
            'status' => $status,
            'deelnemers' => $deelnemers->get()
        ];
    }

    public function deelnemer($id, $startnummer)
    {
        $query = [
            'id' => $id,
            'startnummer' => $startnummer
        ];
        $deelnemer = UitslagDeelnemer::where($query)->first();
        if ($deelnemer !== null) {
            return $deelnemer;
        }

        return UitslagDeelnemerRealTime::where($query)->firstOrFail();
    }
}