<?php

namespace App\Models;


class Categorie extends BaseModel
{
    protected $table = 'uvp_categorie';
    protected $primaryKey = 'cat_id';

    protected $maps = [
        'id' => 'cat_id',
        'omschrijving' => 'cat_oms1',
        'afstands' => 'DATUM',
        'qualificatie' => 'kwal_punten',
    ];

    protected $appends = ['circuit', 'team_run'];

    public function getCircuitAttribute()
    {
        // TODO: correct mapping
        switch($this->cat_code) {
            case 'BSC':
                return 'ksr';
            case 'RUC':
                return 'msr';
            case 'TSC':
                return 'lsr';
            case 'JSR':
                return 'jsr';
            default:
                return null;
        }
    }

    public function getTeamRunAttribute() : bool
    {
        return $this->groepsaantal > 0;
    }
}