<?php

namespace App\Models;


class UitslagDeelnemer extends BaseModel
{
    protected $table = 'uvpu_uitslag';
    protected $primaryKey = 'ID';

    protected $maps = [
        'wedstrijd_id' => 'WEDSTRIJD_ID',
        'startnummer' => 'STARTNR',
        'positie' => 'POSITIE',
        'looptijd' => 'LOOPTIJD',
        'hindernis' => 'HINDERNIS',
        'punten' => 'PUNTEN',
        'voornaam' => 'VOORNAAM',
        'tussenvoegsel' => 'TUSSENVOEGSEL',
        'achternaam' => 'ACHTERNAAM',
        'woonplaats' => 'WOONPLAATS',
        'geslacht' => 'GESLACHT',
        'team' => 'GROEPNAAM',

    ];

    protected $appends = ['positie_veteraan'];

    public function getPositieVeteraanAttribute()
    {
        // TODO
        return null;
    }
}