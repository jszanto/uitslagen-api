<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Sofa\Eloquence\Validable;

class BaseModel extends Model
{

    use Eloquence, Mappable, Validable;

    protected $maps = [];

    /**
     * Returns the array (json) representation of the model
     * Only returns mapped keys
     * @return array
     * @override
     */
    public function toArray()
    {
        $data = [];
        foreach ($this->maps as $key => $map) {
            $data[$key] = $this->$map;
        }

        foreach ($this->appends as $key) {
            $data[$key] = $this->$key;
        }

        return $data;
    }

}
