<?php
namespace App\Models;


class Wedstrijd extends BaseModel
{
    protected $table = 'uvp_wedstrijd';
    protected $primaryKey = 'ID';

    protected $maps = [
        'id' => 'ID',
        'organisator' => 'ORGANISATOR',
        'datum' => 'DATUM',
        'naam' => 'WEDSTRIJD_NAAM',
        'plaats' => 'PLAATS',
        'url' => 'URL',
    ];

    protected $appends = ['inschrijf_url', 'circuit_types'];

    public function getInschrijfUrlAttribute()
    {
        // TODO: kunnen we deze genereren op een manier dat hij altijd klopt?
        return 'https://www.uvponline.nl/uvponlineF/inschrijven/' . $this->ID;
    }

    public function getCircuitTypesAttribute()
    {
        $types = [];
        if ($this->BSC) {
            $types[] = 'ksr';
        }
        if ($this->RUC) {
            $types[] = 'msr';
        }
        if ($this->TSC) {
            $types[] = 'lsr';
        }
        if ($this->JSC) {
            $types[] = 'jsr';
        }

        return $types;
    }
}